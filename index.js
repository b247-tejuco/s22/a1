let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList=[];

//Part 1

    function register(user){
        if(registeredUsers.includes(user)){
            alert("Registration failed. Username already exists.");
            
        } else{
            registeredUsers.push(user);
            alert("Thank you for registering.");
        }
    }
    register("Conan O' Brien");
    console.log(registeredUsers);

//Part 2

    function addFriend(user){
        if(registeredUsers.includes(user)){
            friendsList.push(user);
            alert('You have added '+user+' as a friend!')
        } else{
            alert('User "'+user +'" not found.')
        }
    }
    addFriend("Akiko Yukihime");
    console.log(friendsList);
    addFriend("Conan O' Brien");
    console.log(friendsList);
    addFriend("James Gunn");
    console.log(friendsList);

//Part 3
    
    function showFriends(){
        if(friendsList.length === 0){
            alert('You currently have 0 friends. Add one first.')
        } else{
            for(let i=0;i<friendsList.length;i++){
                console.log(friendsList[i]);
            }
        }
    }
    showFriends();


//Part 4
    function countFriends(){
        
        if (friendsList.length === 0){
            alert('You currently have 0 friends. Add one first.')
        } else{
            alert("You currently have "+friendsList.length+" friends.")
        }
    }

    countFriends();

//Part 5

/*    function deleteLastAddedFriend(){
        if (friendsList.length === 0){
            alert('You currently have 0 friends. Add one first.')
        } else {
            friendsList.pop()
        }
    }
        deleteLastAddedFriend();
        console.log(friendsList);*/


    //stretch goal

        function deleteFriend(index){
        if (friendsList.length === 0){
            alert('You currently have 0 friends. Add one first.')
        }else if(index === 0){
            friendsList.splice(0,1)
        }else if(index === 1){
            friendsList.splice(1,1)
        } else{
            friendsList.pop();
        }
    }
        deleteFriend(1);//input the index number of friend to be deleted.
        console.log(friendsList);





